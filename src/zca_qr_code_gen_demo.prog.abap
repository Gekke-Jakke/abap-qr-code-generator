*&---------------------------------------------------------------------*
*& Report  ZCA_QR_CODE_GEN_DEMO
*&
*&---------------------------------------------------------------------*
*&
*&
*&---------------------------------------------------------------------*
REPORT zca_qr_code_gen_demo.

SELECTION-SCREEN BEGIN OF BLOCK bl0 WITH FRAME TITLE text-000.
PARAMETERS: p_data      TYPE string OBLIGATORY,
            p_format(4) TYPE c,
            p_size      TYPE int4,
            p_http      TYPE boolean AS CHECKBOX.
SELECTION-SCREEN END OF BLOCK bl0.

CLASS lcl_demo DEFINITION.
  PUBLIC SECTION.
    CLASS-METHODS: display_qr_code.

ENDCLASS.

CLASS lcl_demo IMPLEMENTATION.

  METHOD display_qr_code.
    DATA(lo_qr_gen) = zcl_qr_generator=>get_instance( ).

    TRY.
*     Set QR code data
      lo_qr_gen->set_data( to_lower( p_data ) ).

*     Set image format
      IF p_format IS NOT INITIAL.
        lo_qr_gen->set_format( CONV string( to_lower( p_format ) ) ).
      ENDIF.

*     Set image size
      IF p_size IS NOT INITIAL.
        lo_qr_gen->set_size( p_size ).
      ENDIF.

*     Display preview
      lo_qr_gen->preview( p_http ).

*     Direct call without preview can be made using following method
*      lo_qr_gen->get_qr_code(
*        EXPORTING
*          iv_http          =     " HTTP instead of HTTPS
*        IMPORTING
*          ev_image         =     " QR code (binary format)
*          ev_mimetype      =     " Mimetype
*      ).
*        CATCH zcx_qr_generator.    "

    CATCH zcx_qr_generator INTO DATA(lcx_qr_generator).
      DATA(lv_message) = lcx_qr_generator->get_text( ).
      MESSAGE lv_message TYPE 'E'.
    ENDTRY.

  ENDMETHOD.

ENDCLASS.

START-OF-SELECTION.
  lcl_demo=>display_qr_code( ).
