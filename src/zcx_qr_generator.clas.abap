class ZCX_QR_GENERATOR definition
  public
  inheriting from CX_STATIC_CHECK
  final
  create public .

public section.

  interfaces IF_T100_MESSAGE .

  constants:
    begin of ZCX_QR_GENERATOR,
      msgid type symsgid value 'ZCA_QR_GENERATOR',
      msgno type symsgno value '000',
      attr1 type scx_attrname value '',
      attr2 type scx_attrname value '',
      attr3 type scx_attrname value '',
      attr4 type scx_attrname value '',
    end of ZCX_QR_GENERATOR .
  constants:
    begin of FAILED_INSTANTIATE_HTTP,
      msgid type symsgid value 'ZCA_QR_GENERATOR',
      msgno type symsgno value '001',
      attr1 type scx_attrname value '',
      attr2 type scx_attrname value '',
      attr3 type scx_attrname value '',
      attr4 type scx_attrname value '',
    end of FAILED_INSTANTIATE_HTTP .
  constants:
    begin of FAILED_SEND_TO_API,
      msgid type symsgid value 'ZCA_QR_GENERATOR',
      msgno type symsgno value '002',
      attr1 type scx_attrname value '',
      attr2 type scx_attrname value '',
      attr3 type scx_attrname value '',
      attr4 type scx_attrname value '',
    end of FAILED_SEND_TO_API .
  constants:
    begin of FAILED_RECEIVE_FROM_API,
      msgid type symsgid value 'ZCA_QR_GENERATOR',
      msgno type symsgno value '003',
      attr1 type scx_attrname value '',
      attr2 type scx_attrname value '',
      attr3 type scx_attrname value '',
      attr4 type scx_attrname value '',
    end of FAILED_RECEIVE_FROM_API .
  constants:
    begin of ILLEGAL_FORMAT,
      msgid type symsgid value 'ZCA_QR_GENERATOR',
      msgno type symsgno value '005',
      attr1 type scx_attrname value '',
      attr2 type scx_attrname value '',
      attr3 type scx_attrname value '',
      attr4 type scx_attrname value '',
    end of ILLEGAL_FORMAT .
  constants:
    begin of WRONG_SIZE,
      msgid type symsgid value 'ZCA_QR_GENERATOR',
      msgno type symsgno value '004',
      attr1 type scx_attrname value '',
      attr2 type scx_attrname value '',
      attr3 type scx_attrname value '',
      attr4 type scx_attrname value '',
    end of WRONG_SIZE .

  methods CONSTRUCTOR
    importing
      !TEXTID like IF_T100_MESSAGE=>T100KEY optional
      !PREVIOUS like PREVIOUS optional .
protected section.
private section.
ENDCLASS.



CLASS ZCX_QR_GENERATOR IMPLEMENTATION.


  method CONSTRUCTOR.
CALL METHOD SUPER->CONSTRUCTOR
EXPORTING
PREVIOUS = PREVIOUS
.
clear me->textid.
if textid is initial.
  IF_T100_MESSAGE~T100KEY = ZCX_QR_GENERATOR .
else.
  IF_T100_MESSAGE~T100KEY = TEXTID.
endif.
  endmethod.
ENDCLASS.
