class ZCL_QR_GENERATOR definition
  public
  final
  create private .

public section.

  constants GC_FORMAT_PNG type STRING value 'png' ##NO_TEXT.
  constants GC_FORMAT_GIF type STRING value 'gif' ##NO_TEXT.
  constants GC_FORMAT_JPEG type STRING value 'jpeg' ##NO_TEXT.
  constants GC_FORMAT_JPG type STRING value 'jpg' ##NO_TEXT.
  constants GC_FORMAT_SVG type STRING value 'svg' ##NO_TEXT.
  constants GC_FORMAT_EPS type STRING value 'eps' ##NO_TEXT.

  class-methods GET_INSTANCE
    returning
      value(RO_QR_GENERATOR) type ref to ZCL_QR_GENERATOR .
  methods SET_SIZE
    importing
      !IV_SIZE type INT4
    raising
      ZCX_QR_GENERATOR .
  methods SET_FORMAT
    importing
      !IV_FORMAT type STRING
    raising
      ZCX_QR_GENERATOR .
  methods SET_DATA
    importing
      !IV_DATA type STRING .
  methods GET_QR_CODE
    importing
      !IV_HTTP type BOOLEAN optional
    exporting
      !EV_IMAGE type XSTRING
      !EV_MIMETYPE type STRING
    raising
      ZCX_QR_GENERATOR .
  methods PREVIEW
    importing
      !IV_HTTP type BOOLEAN optional
    raising
      ZCX_QR_GENERATOR .
  PROTECTED SECTION.
private section.

  class-data GO_INSTANCE type ref to ZCL_QR_GENERATOR .
  data GV_FORMAT type STRING value GC_FORMAT_PNG ##NO_TEXT.
  data GV_SIZE type STRING value '150x150' ##NO_TEXT.
  data GV_DATA type STRING .
ENDCLASS.



CLASS ZCL_QR_GENERATOR IMPLEMENTATION.


  METHOD get_instance.
*   Allow only one instance of the QR code generator (singleton)
    IF go_instance IS NOT BOUND.
      go_instance = NEW zcl_qr_generator( ).
    ENDIF.

*   Return instance
    ro_qr_generator = go_instance.

  ENDMETHOD.


  METHOD get_qr_code.
    CONSTANTS: lc_http TYPE string VALUE 'http://',
               lc_https TYPE string VALUE 'https://',
               lc_api_url TYPE string VALUE 'api.qrserver.com/v1/create-qr-code/?data='.
    DATA lv_api_request_url TYPE string.

*   URL encode data string
    DATA(lv_data) = cl_http_utility=>escape_url(
      EXPORTING
        unescaped = gv_data    " Unencoded String
    ).

*   Concatenate API request url
    IF iv_http IS INITIAL.
      lv_api_request_url = lc_https && lc_api_url.
    ELSE.
      lv_api_request_url = lc_http && lc_api_url.
    ENDIF.

*   Set data
    lv_api_request_url = lv_api_request_url && lv_data.

*   Set format
    IF gv_format IS NOT INITIAL.
      lv_api_request_url = lv_api_request_url && '&format=' && gv_format.
    ENDIF.

*   Set size
    IF gv_size IS NOT INITIAL.
      lv_api_request_url = lv_api_request_url && '&size=' && gv_size.
    ENDIF.

*   Create HTTP client object with url
    cl_http_client=>create_by_url(
      EXPORTING
        url                = CONV string( lv_api_request_url )   " URL
       IMPORTING
         client             = DATA(lo_http_client)    " HTTP Client Abstraction
       EXCEPTIONS
         argument_not_found = 1
         plugin_not_active  = 2
         internal_error     = 3
         OTHERS             = 4 ).
    IF sy-subrc <> 0.
      RAISE EXCEPTION TYPE zcx_qr_generator
        EXPORTING
          textid = zcx_qr_generator=>failed_instantiate_http.
    ENDIF.

*   Set HTTP method to GET
    lo_http_client->request->set_method( method = if_http_request=>co_request_method_get ).

*   Send the HTTP request to the API
    lo_http_client->send(
      EXPORTING
        timeout                    = 10    " Timeout of Answer Waiting Time
       EXCEPTIONS
         http_communication_failure = 1
         http_invalid_state         = 2
         http_processing_failed     = 3
         http_invalid_timeout       = 4
         OTHERS                     = 5 ).

    IF sy-subrc <> 0.
      RAISE EXCEPTION TYPE zcx_qr_generator
        EXPORTING
          textid = zcx_qr_generator=>failed_send_to_api.
    ENDIF.

*   Receive response from API
    lo_http_client->receive(
      EXCEPTIONS
        http_communication_failure = 1
        http_invalid_state         = 2
        http_processing_failed     = 3
        OTHERS                     = 4 ).

    IF sy-subrc <> 0.
      RAISE EXCEPTION TYPE zcx_qr_generator
        EXPORTING
          textid = zcx_qr_generator=>failed_receive_from_api.
    ENDIF.

*   Get QR code from response
    ev_image = lo_http_client->response->get_data( ).
    ev_mimetype = lo_http_client->response->get_content_type( ).

  ENDMETHOD.


  METHOD preview.
    DATA: lv_html     TYPE string,
          lv_image    TYPE xstring,
          lv_mimetype TYPE string,
          lv_base64_image TYPE string.

*   Get QR code
    me->get_qr_code(
      EXPORTING
        iv_http          = iv_http    " HTTP instead of HTTPS
      IMPORTING
        ev_image         = lv_image    " QR code (binary format)
        ev_mimetype      = lv_mimetype    " Mimetype
    ).

*   Convert binary to base64 encoding
    CALL FUNCTION 'SSFC_BASE64_ENCODE'
      EXPORTING
        bindata                  = lv_image
*       BINLENG                  =
      IMPORTING
        b64data                  = lv_base64_image
      exceptions
        ssf_krn_error            = 1
        ssf_krn_noop             = 2
        ssf_krn_nomemory         = 3
        ssf_krn_opinv            = 4
        ssf_krn_input_data_error = 5
        ssf_krn_invalid_par      = 6
        ssf_krn_invalid_parlen   = 7
        OTHERS                   = 8.
    IF sy-subrc <> 0.
* Implement suitable error handling here
    ENDIF.

*   Prepare HTML preview
    lv_html = '<!doctype html>'.
    lv_html = lv_html && '<html lang="' && sy-langu && '">'.
    lv_html = lv_html && '<head><title>QR Code Preview</title></head>'.
    lv_html = lv_html && '<body>'.
    lv_html = lv_html && '<img src="data:' && lv_mimetype && ';base64,'
                      && lv_base64_image
                      && '"/><br />'.
    lv_html = lv_html && '</body></html>'.

*   Show preview in popup
    cl_abap_browser=>show_html(
                     title        = CONV cl_abap_browser=>title( 'QR Code Preview' )     " Window Title
                     size         = cl_abap_browser=>medium    " Size (S,M.L,XL)
                     modal        = abap_true  " Display as Modal Dialog Box
                     html_string  = lv_html ). " HTML String

  ENDMETHOD.


  METHOD set_data.

*   Set QR code data
    gv_data = iv_data.

  ENDMETHOD.


  METHOD set_format.

*   In case an illegal format is set throw exception
    IF iv_format <> gc_format_eps
      AND iv_format <> gc_format_gif
      AND iv_format <> gc_format_jpeg
      AND iv_format <> gc_format_jpg
      AND iv_format <> gc_format_png
      AND iv_format <> gc_format_svg.

      RAISE EXCEPTION TYPE zcx_qr_generator
        EXPORTING
          textid = zcx_qr_generator=>illegal_format.

    ELSE.
      gv_format = iv_format.
    ENDIF.

  ENDMETHOD.


  METHOD set_size.

*   API is limited to 1000x1000 pixels, except for EPS and SVG formats
    IF iv_size > 1000 AND gv_format <> gc_format_eps AND gv_format <> gc_format_svg.
      RAISE EXCEPTION TYPE zcx_qr_generator
        EXPORTING
          textid = zcx_qr_generator=>wrong_size.
    ELSE.
      gv_size = iv_size && 'x' && iv_size.
    ENDIF.

  ENDMETHOD.
ENDCLASS.
