<B>ABAP QR Code Generator</B>

ABAP QR Code Generator using goqr.me service.<BR />
Installation can be done using abapGit.

Additional configuration might be required based on your system setup when using the default HTTPS connection. (STRUST configuration for api.qrserver.com) <BR/>
HTTPS can be disabled by setting parameter iv_http to abap_true.
<BR/><BR/>
Demo report available ZCA_QR_CODE_GEN_DEMO.